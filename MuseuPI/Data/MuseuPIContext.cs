﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MuseuPI.Models
{
    public class MuseuPIContext : IdentityDbContext
    {
        public MuseuPIContext(DbContextOptions<MuseuPIContext> options)
       : base(options)
        {

        }

        public DbSet<Item> Itens { get; set; }
        public DbSet<Colecao> Colecoes { get; set; }
        public DbSet<Midia> Midias { get; set; }
        public DbSet<Procedencia> Procedencias { get; set; }
    }
}
