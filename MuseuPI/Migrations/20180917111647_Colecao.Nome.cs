﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MuseuPI.Migrations
{
    public partial class ColecaoNome : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Nome",
                table: "Colecoes",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Nome",
                table: "Colecoes");
        }
    }
}
