﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MuseuPI.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Colecoes",
                columns: table => new
                {
                    ColecaoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    DataUltimaModificacao = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Colecoes", x => x.ColecaoId);
                });

            migrationBuilder.CreateTable(
                name: "Procedencias",
                columns: table => new
                {
                    ProcedenciaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(nullable: true),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    DataUltimaModificacao = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procedencias", x => x.ProcedenciaId);
                });

            migrationBuilder.CreateTable(
                name: "Itens",
                columns: table => new
                {
                    ItemId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ColecaoId = table.Column<int>(nullable: false),
                    ProcedenciaId = table.Column<int>(nullable: false),
                    Conservacao = table.Column<int>(nullable: false),
                    Titulo = table.Column<string>(maxLength: 100, nullable: true),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    DataUltimaModificacao = table.Column<DateTime>(nullable: true),
                    DataFabricacao = table.Column<DateTime>(nullable: false),
                    DataAquisicao = table.Column<DateTime>(nullable: false),
                    Descricao = table.Column<string>(nullable: true),
                    Funcao = table.Column<string>(maxLength: 100, nullable: true),
                    MaterialTecnica = table.Column<string>(maxLength: 100, nullable: true),
                    Historico = table.Column<string>(nullable: true),
                    Peso = table.Column<decimal>(nullable: false),
                    Largura = table.Column<decimal>(nullable: false),
                    Profundidade = table.Column<decimal>(nullable: false),
                    Altura = table.Column<decimal>(nullable: false),
                    Preco = table.Column<decimal>(nullable: false),
                    DataAvaliacao = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Itens", x => x.ItemId);
                    table.ForeignKey(
                        name: "FK_Itens_Colecoes_ColecaoId",
                        column: x => x.ColecaoId,
                        principalTable: "Colecoes",
                        principalColumn: "ColecaoId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Itens_Procedencias_ProcedenciaId",
                        column: x => x.ProcedenciaId,
                        principalTable: "Procedencias",
                        principalColumn: "ProcedenciaId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Midias",
                columns: table => new
                {
                    MidiaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Texto = table.Column<string>(nullable: true),
                    TipoMidia = table.Column<int>(nullable: false),
                    ItemId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Midias", x => x.MidiaId);
                    table.ForeignKey(
                        name: "FK_Midias_Itens_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Itens",
                        principalColumn: "ItemId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Itens_ColecaoId",
                table: "Itens",
                column: "ColecaoId");

            migrationBuilder.CreateIndex(
                name: "IX_Itens_ProcedenciaId",
                table: "Itens",
                column: "ProcedenciaId");

            migrationBuilder.CreateIndex(
                name: "IX_Midias_ItemId",
                table: "Midias",
                column: "ItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Midias");

            migrationBuilder.DropTable(
                name: "Itens");

            migrationBuilder.DropTable(
                name: "Colecoes");

            migrationBuilder.DropTable(
                name: "Procedencias");
        }
    }
}
