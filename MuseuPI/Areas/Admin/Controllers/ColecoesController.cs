﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using MuseuPI.Models;

namespace MuseuPI.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ColecoesController : Controller
    {

        protected readonly MuseuPIContext _context;

        public ColecoesController(MuseuPIContext context)
        {
            _context = context;
        }

        // GET: Admin/Colecoes
        public async Task<IActionResult> Index()
        {
            ViewBag.Confirm = TempData["Confirm"];
            return View(await _context.Colecoes.ToListAsync());
        }

        // GET: Admin/Colecoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var colecao = await _context.Colecoes
                .SingleOrDefaultAsync(m => m.ColecaoId == id);
            if (colecao == null)
            {
                return NotFound();
            }

            return View(colecao);
        }

        // GET: Admin/Colecoes/Create
        public IActionResult Create()
        {
            var colecao = new Colecao();
            return View(colecao);
        }

        // POST: Admin/Colecoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ColecaoId,Nome,DataCadastro,DataUltimaModificacao")] Colecao colecao)
        {
            if (ModelState.IsValid)
            {
                TempData["Confirm"] = "<script>$(document).ready(function () {MostraConfirm('Sucesso', 'Gravado com sucesso.');})</script>";

                _context.Add(colecao);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(colecao);
        }

        // GET: Admin/Colecoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var colecao = await _context.Colecoes.SingleOrDefaultAsync(m => m.ColecaoId == id);
            if (colecao == null)
            {
                return NotFound();
            }
            return View(colecao);
        }

        // POST: Admin/Colecoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ColecaoId,Nome,DataCadastro,DataUltimaModificacao")] Colecao colecao)
        {
            if (id != colecao.ColecaoId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    TempData["Confirm"] = "<script>$(document).ready(function () {MostraConfirm('Sucesso', 'Alterado com sucesso.');})</script>";

                    var db = _context.Colecoes.Find(id);
                    _context.Entry(db).CurrentValues.SetValues(colecao);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ColecaoExists(colecao.ColecaoId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(colecao);
        }

        // GET: Admin/Colecoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var colecao = await _context.Colecoes
                .SingleOrDefaultAsync(m => m.ColecaoId == id);
            if (colecao == null)
            {
                return NotFound();
            }

            return View(colecao);
        }

        // POST: Admin/Colecoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            TempData["Confirm"] = "<script>$(document).ready(function () {MostraConfirm('Sucesso', 'Deletado com sucesso.');})</script>";
            var colecao = await _context.Colecoes.SingleOrDefaultAsync(m => m.ColecaoId == id);
            _context.Colecoes.Remove(colecao);
            await _context.SaveChangesAsync();


            return RedirectToAction(nameof(Index));
        }

        private bool ColecaoExists(int id)
        {
            return _context.Colecoes.Any(e => e.ColecaoId == id);
        }
    }
}
