﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MuseuPI.Models
{
    public interface IStatusModificacao
    {
        DateTime DataCadastro { get; set; }

        DateTime? DataUltimaModificacao { get; set; }
    }
}
