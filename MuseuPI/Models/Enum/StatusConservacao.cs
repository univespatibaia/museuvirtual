﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MuseuPI.Models.Enum
{
    public enum StatusConservacao
    {
        [Display(Name = "Selecione estado de conservação")]
        Nenhum = 0,
        [Display(Name = "Ruim")]
        Ruim = 1,
        [Display(Name = "Baixo")]
        Baixo = 2,
        [Display(Name = "Médio")]
        Medio = 3,
        [Display(Name = "Bom")]
        Bom = 4,
        [Display(Name = "Excelente")]
        Excelente = 5


    }
}
