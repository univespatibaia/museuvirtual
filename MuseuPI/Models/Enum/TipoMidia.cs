﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MuseuPI.Models.Enum
{
    public enum TipoMidia
    {
        [Display(Name = "Selecione tipo de mídia")]
        Nenhum = 0,
        [Display(Name = "Foto")]
        Foto = 1,
        [Display(Name = "Youtube")]
        Youtube,
        [Display(Name = "Mp3")]
        Mp3,
        [Display(Name = "Documento")]
        Documento,
    }
}
