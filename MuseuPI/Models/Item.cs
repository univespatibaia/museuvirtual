﻿using MuseuPI.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MuseuPI.Models
{
    public class Item : IStatusModificacao
    {
        public int ItemId { get; set; }

        public int ColecaoId { get; set; }

        public int ProcedenciaId { get; set; }

        public StatusConservacao Conservacao { get; set; }

        [MaxLength(100, ErrorMessage = "{0} pode ter no máximo {1} caracteres")]
        public string Titulo { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        [Display(Name = "Data Cadastro")]
        public DateTime DataCadastro { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        [Display(Name = "Data Últ. Modificação")]
        public DateTime? DataUltimaModificacao { get; set; }
        [Display(Name = "Data Fabricação")]
        [DataType(DataType.Date)]
        public DateTime DataFabricacao { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Data Aquisição")]
        public DateTime DataAquisicao { get; set; }

        public string Descricao { get; set; }

        [Display(Name = "Função")]
        [MaxLength(100, ErrorMessage = "{0} pode ter no máximo {1} caracteres")]
        public string Funcao { get; set; }

        [Display(Name = "Material/Técnica")]
        [MaxLength(100, ErrorMessage = "{0} pode ter no máximo {1} caracteres")]
        public string MaterialTecnica { get; set; }

        public string Historico { get; set; }


        public decimal Peso { get; set; }
        public decimal Largura { get; set; }
        public decimal Profundidade { get; set; }
        public decimal Altura { get; set; }

        [Display(Name = "Preço")]
        public decimal Preco { get; set; }
        [Display(Name = "Data Avaliação")]
        public DateTime DataAvaliacao { get; set; }
        public virtual Colecao Colecao { get; set; }
        public virtual Procedencia Procedencia { get; set; }

        public virtual ICollection<Midia> Midias { get; set; }

    }
}
