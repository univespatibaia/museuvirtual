﻿using MuseuPI.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MuseuPI.Models
{
    public class Midia
    {
        public int MidiaId { get; set; }

        public string Texto { get; set; }
        public TipoMidia TipoMidia { get; set; }

        public int ItemId { get; set; }


        public virtual Item Item { get; set; }
    }
}
